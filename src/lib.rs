use regex::Regex;
use std::collections::HashMap;
use std::process::Command;

pub fn get_vm_info(name: &str) -> HashMap<String, String> {
    let mut info: HashMap<String, String> = HashMap::new();

    let regex = Regex::new(r"\s").unwrap();

    let output = Command::new("vboxmanage")
        .args(["showvminfo", name])
        .output()
        .unwrap();

    let output = String::from_utf8(output.stdout).unwrap();

    for x in output.split("\n") {
        if let Some((key, value)) = x.split_once(":") {
            let key = regex.replace_all(key, "_").to_string();
            info.insert(key.to_owned(), value.trim().to_owned());
        }
    }

    info
}
